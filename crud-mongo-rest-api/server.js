//Imports
const express = require('express');
const logger = require('morgan');
const errorHandler = require('errorhandler');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');

//configuraciones
const url = 'mongodb://localhost:27017/accounts';

//instanciaciones
let app = express();


//middlewares
app.use(bodyParser.json());
app.use(logger('dev'));

//servidor

mongodb.MongoClient.connect(url, {useUnifiedTopology: true},(error, client) => {

  if (error) {return process.exit(1);}
  var db = client.db('accounts');
  //montaje
  app.get('/accounts', (req,res) => {
    db.collection('accounts')
    .find({}, {sort: {_id: -1}})
    .toArray((error, accounts) =>{
      if (error) {return next(error);}
      res.send(accounts).status(200);
    });
  });

  app.post('/accounts', (req, res) => {
    let nuevaCuenta = req.body;
    db.collection('accounts').insert(nuevaCuenta, (error, results) => {
      if (error) {return next(error);}
      res.send(results).status(201);
    });
  });

  app.put('/:id/accounts', (req, res) =>{
    db.collection('accounts').update({
      _id: mongodb.ObjectID(req.params.id)},
      {$set: req.body},
      (error, results) => {
        if (error) {return next(error);}
        res.send(results).status(200);
      });
  });

  app.delete('/:id/accounts', (req, res) =>{
    db.collection('accounts').deleteOne({
      _id: mongodb.ObjectID(req.params.id)},
      (error, results) => {
        if(error){return next(error);}
        res.send(results).status(204);
      }
    );
  });

});

app.listen(3000);
